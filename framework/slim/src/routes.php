<?php
// Routes

use Slim\Http\Request;
use Slim\Http\Response;

$app->get('/[{name}]', function ($request, $response, $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});

/**
 * @todo extract to class and interface
 */
$routeConverter = new class ()
{
	/**
	 * @param string $routeName
	 *
	 * @return string
	 */
	public static function convertRoute(string $routeName): string
	{
		return preg_replace('#/:([\w]+)#', '/{$1}', $routeName);
	}
};

$configuration = new \GameOfLife\GameOfLifeConfiguration();
$configuration->setCellFactory(new \GameOfLife\Component\Cell\NeighboringCellFactory());
$configuration->setRuleSet(new \GameOfLife\Component\Rule\World\WorldRuleSet());
$gameOfLifeRouter = new \GameOfLife\Api\Routing\GameOfLifeRouter($configuration);
$routes           = $gameOfLifeRouter->getRouteList();

foreach ($routes as $routeName => $routeCallable)
{
	/**
	 * @param Request  $request
	 * @param Response $response
	 * @param array    $args
	 *
	 * @return mixed
	 */
	$callable = function (Request $request, Response $response, $args) use ($routeCallable)
	{
		if ($request->getMethod() == 'GET')
		{
			return $this->renderer->render(
				$response, 'game-of-life.phtml',
				['response' => call_user_func_array($routeCallable, $args),
					'args' => $args]
			);
		}
		return $response->withJson(["response" => call_user_func_array($routeCallable, $args)], 200);
	};
	$app->get($routeConverter::convertRoute($routeName), $callable);
	$app->post($routeConverter::convertRoute($routeName), $callable);
}
