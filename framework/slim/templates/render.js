function renderGrid() {
	var canvasName = 'gameCanvas';
	var worldObject = [];
	var canvasObject = document.getElementById(canvasName);

	/** we can modify this later */
	var cellBorder = 3;

	/**
	 * Lets imagine we get correct values. What we need?
	 * Using data-x-width and data-y-height
	 */
	var $x = canvasObject.dataset.xWidth;
	var $y = canvasObject.dataset.yHeight;

	/**
	 * Colors
	 */
	var aliveColor = "green";
	var deadColor  = "red";

	var canvas = document.getElementById(canvasName);
	var con = canvas.getContext('2d');
	/**
	 * Defined by effectiveCanvasCalculation
	 * @type {number}
	 */
	var effectiveCellSize = 0;

	canvas.style.background = '#000';
	document.body.appendChild(canvas);

	/**
	 * Increase canvas size to screen Width and Height
	 */
	function canvasCalculations() {
		canvas.width = screen.width;
		canvas.height = screen.height;

		effectiveSize = effectiveCanvasCalculation();
		canvas.width = effectiveSize * $x;
		canvas.height = effectiveSize * $y;
	}

	function effectiveCanvasCalculation() {
		var effectiveCellWidth = parseInt(screen.width / $x);
		var effectiveCellHeight = parseInt(screen.height / $y);
		effectiveCellSize = effectiveCellHeight > effectiveCellWidth ? effectiveCellWidth : effectiveCellHeight;

		return effectiveCellSize;
	}

	/**
	 * Colors the cell. Simulates "border"
	 *
	 * @param x
	 * @param y
	 * @param color
	 */
	function colorCell(x, y, color)
	{
		var border = cellBorder;
		var c = document.getElementById(canvasName);
		var ctx = c.getContext("2d");

		var myX = x * effectiveCellSize;
		var myY = y * effectiveCellSize;

		ctx.fillStyle=color;
		ctx.fillRect(myX + border, myY + border, effectiveCellSize - border, effectiveCellSize - border);
	}

	/**
	 * Simple iteration to color world
	 */
	function colorWorld()
	{
		var width = $x;
		var height = $y;
		console.log($x, $y);

		for (var i = 0; i < width; i++)
		{
			console.log(height);
			for (var j = 0; j < height; j++)
			{
				colorCell(i, j, worldObject[j][i] == 'A' ? aliveColor : deadColor);
			}
		}
	}

	/**
	 * Grid Lines
	 */
	function grid(x, y) {
		/**
		 * very crude draw function
		 *
		 * @param i
		 * @param size
		 */
		function drawLines(i, size)
		{
			con.moveTo(i, 0);
			con.lineTo(i, size);
			// draw vertical lines
			con.moveTo(0, i);
			con.lineTo(size, i);
		}
		var w = canvas.width,
			h = canvas.height,
			cellSize = effectiveCanvasCalculation(),
			size = cellSize * x;

		/**
		 * i is used for both x and y to draw
		 * a line every 5 pixels starting at
		 * .5 to offset the canvas edges
		 */
		for (var i = .5; i <= size + .5; i += cellSize) {
			drawLines(i, size);
		}
		con.strokeStyle = 'hsla(0, 0%, 40%, .5)';
		con.stroke();
	}

	function setup()
	{
		worldObject = JSON.parse(world);
		console.log(worldObject)
	}

	setup();

	canvasCalculations();
	grid($x, $y);
	colorWorld();
}
