<?php

namespace GameOfLife;

use GameOfLife\Component\World;

class GameOfLife
{
	const GOL_EMPTY_CELL = 0;

	/**
	 * Current life cycle number
	 *
	 * @var int
	 */
	private $lifeCycle = 0;

	/**
	 * @var GameOfLifeConfiguration
	 */
	private $configuration;

	/**
	 * Contains every cell of the world in an array
	 *
	 * @var World
	 */
	private $world;

	/**
	 * GameOfLife constructor.
	 *
	 * @param GameOfLifeConfiguration $configuration
	 */
	public function __construct(GameOfLifeConfiguration $configuration = null)
	{
		$this->configuration = $configuration;

		$this->createWorld();
	}

	/**
	 * Iterates one life cycle.
	 * Returns new current life cycle number.
	 *
	 * @return int
	 */
	public function iterate()
	{
		/**
		 * @todo game life cycle iteration
		 */

		return ++$this->lifeCycle;
	}

	/**
	 * Renders current life cycle result.
	 *
	 * @return mixed
	 */
	public function display()
	{
		/**
		 * @todo game life cycle display logic
		 */

		return $this->world;
	}

	/**
	 * Executes life iteration and returns the result.
	 * Combination of iterate and show.
	 *
	 * @return mixed
	 */
	public function play()
	{
		$this->iterate();

		/**
		 * @todo game life cycle analysis here if we want to change things before output
		 */

		return $this->display();
	}

	/**
	 * Creates world for the game using configuration field size.
	 */
	private function createWorld()
	{
		$fieldSize = $this->configuration->getFieldSize();


		$this->world = new World($fieldSize);
	}
}
