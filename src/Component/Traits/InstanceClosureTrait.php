<?php

namespace GameOfLife\Component\Traits;

use Closure;

/**
 * Trait InstanceClosureTrait
 *
 * Adds functionality to access class instance at any point via closure.
 */
trait InstanceClosureTrait
{
	/**
	 * Returns closure to allow access to this cell
	 *
	 * @return Closure
	 */
	public function getInstanceClosure()
	{
		return function ()
		{
			return $this;
		};
	}
}
