<?php

namespace GameOfLife\Component;

use GameOfLife\Component\Rule\IteratorRule;
use GameOfLife\Component\Rule\RuleFactory;
use GameOfLife\Component\Rule\World\AbstractRuleSet;
use GameOfLife\Component\World\World;
use GameOfLife\Component\World\WorldIterator;
use GameOfLife\GameOfLifeConfiguration;

/**
 * Class GameOfLifeEngine
 *
 * Engine is like an API.
 */
class GameOfLifeEngine
{
	/**
	 * @var World
	 */
	private $world;

	/**
	 * @var GameOfLifeConfiguration
	 */
	private $configuration;

	/**
	 * GameOfLifeEngine constructor.
	 *
	 * If we pass in world, then next state of it can be evaluated.
	 *
	 * @param GameOfLifeConfiguration $configuration
	 *
	 */
	public function __construct(GameOfLifeConfiguration $configuration)
	{
		$this->configuration = $configuration;
	}

	/**
	 * @todo Consider injecting world
	 *
	 * @return World
	 */
	public function getWorld(): World
	{
		if ($this->world === null)
		{
			$this->world = $this->createWorld();
		}

		return $this->world;
	}

	/**
	 * @return World
	 */
	public function createWorld()
	{
		$this->world = new World($this->configuration->getFieldSize(), $this->configuration->getCellFactory());

		return $this->world;
	}

	/**
	 * @return WorldIterator
	 */
	public function createWorldIterator()
	{
		return new WorldIterator($this->getWorld());
	}

	/**
	 * @return RuleFactory
	 */
	public function createRuleFactory()
	{
		return new RuleFactory($this->getRuleSet());
	}

	/**
	 * @return AbstractRuleSet
	 */
	public function getRuleSet()
	{
		return $this->configuration->getRuleSet();
	}

	/**
	 * Applies a single rule on the world.
	 *
	 * @param string $ruleName
	 */
	public function applyRule(string $ruleName)
	{
		$worldIterator = $this->createWorldIterator();
		$ruleFactory   = $this->createRuleFactory();

		$rule         = $ruleFactory->create($ruleName);
		$iteratorRule = new IteratorRule($worldIterator, $rule);
		$iteratorRule->apply();
	}
}
