<?php

namespace GameOfLife\Component\World;

class WorldCoordinates
{
	/**
	 * @var int
	 */
	private $x;

	/**
	 * @var int
	 */
	private $y;

	/**
	 * WorldCoordinates constructor.
	 *
	 * @param int $x
	 * @param int $y
	 */
	public function __construct($x, $y)
	{
		$this->setX($x);
		$this->setY($y);
	}

	/**
	 * @return int
	 */
	public function getX()
	{
		return $this->x;
	}

	/**
	 * @param int $x
	 *
	 * @return WorldCoordinates
	 */
	public function setX($x)
	{
		$this->x = $x;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getY()
	{
		return $this->y;
	}

	/**
	 * @param int $y
	 *
	 * @return WorldCoordinates
	 */
	public function setY($y)
	{
		$this->y = $y;

		return $this;
	}
}
