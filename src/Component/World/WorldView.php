<?php

namespace GameOfLife\Component\World;

use Closure;
use GameOfLife\Component\Cell\NeighboringCell;
use GameOfLife\Component\Interfaces\View\ViewInterface;

class WorldView implements ViewInterface
{
	/** @var Closure */
	private $cellViewClosure;

	/**
	 * @var WorldIterator
	 */
	private $worldIterator;

	private $output;

	public function __construct(World $world, CLosure $cellViewClosure)
	{
		$this->worldIterator   = new WorldIterator($world);
		$this->cellViewClosure = $cellViewClosure;
	}

	/**
	 * Executes closure on iterator to produce output.
	 *
	 * @return mixed
	 */
	public function view()
	{
		$viewClosure     = $this->cellViewClosure;
		$cellViewClosure = $viewClosure($this->output);

		/** @var NeighboringCell $cell */
		foreach ($this->worldIterator as $cell)
		{
			$cellViewClosure($cell);
		}

		return $this->output;
	}
}
