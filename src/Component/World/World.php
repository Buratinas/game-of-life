<?php

namespace GameOfLife\Component\World;

use GameOfLife\Component\Cell\CellContext;
use GameOfLife\Component\Cell\NeighboringCellFactory;
use GameOfLife\Component\Cell\NeighboringCell;
use GameOfLife\Component\FieldSize;
use GameOfLife\Component\Interfaces\Cell\CellCreationInterface;
use GameOfLife\Component\Interfaces\View\ViewInterface;
use GameOfLife\Component\State\Dead;
use GameOfLife\Component\State\State;

class World implements ViewInterface
{
	/**
	 * @var FieldSize
	 */
	private $fieldSize;

	/**
	 * Cell matrix representing world.
	 *
	 * @var NeighboringCell[][]
	 */
	protected $world;

	/**
	 * @var State
	 */
	private $defaultState;

	/**
	 * @var NeighboringCellFactory
	 */
	private $cellFactory;

	/**
	 * @return NeighboringCellFactory
	 */
	public function getCellFactory()
	{
		return $this->cellFactory;
	}

	/**
	 * @return FieldSize
	 */
	public function getFieldSize()
	{
		return $this->fieldSize;
	}

	/**
	 * @return State
	 */
	public function getDefaultState()
	{
		return $this->defaultState;
	}

	/**
	 * World constructor.
	 *
	 * @param FieldSize              $fieldSize
	 * @param CellCreationInterface $cellFactory
	 */
	public function __construct(FieldSize $fieldSize, CellCreationInterface $cellFactory)
	{
		$this->fieldSize    = $fieldSize;
		$this->defaultState = Dead::state();
		$this->cellFactory  = $cellFactory;

		$this->generateWorldDimensions();
		$this->populateWorld();
	}

	/**
	 * Displays the world.
	 *
	 * @return string
	 */
	public function view()
	{
		$output = '';

		foreach ($this->world as $index => $row)
		{
			foreach ($row as $cell)
			{
				$output .= ($cell->getState() == $this->getDefaultState()) ? 'D' : 'A';
			}
			$output .= PHP_EOL;
		}

		return $output;
	}

	/**
	 * Returns a Cell to manipulate.
	 *
	 * @param int $x
	 * @param int $y
	 *
	 * @return NeighboringCell
	 */
	public function manipulate($x, $y)
	{
		$cellExists = isset($this->world[$x][$y]);

		return $cellExists ? $this->world[$x][$y] : $this->getCellFactory()->createInvalidCell();
	}

	/**
	 * Populates world with predefined state.
	 */
	private function populateWorld()
	{
		foreach ($this->world as $x => $row)
		{
			foreach ($row as $y => $cell)
			{
				// @todo create single CellContext with $this (World) and call method at($x)->at($y) to return modified context
				$cellContext         = new CellContext($this, new WorldCoordinates($x, $y));
				$this->world[$x][$y] = $this->getCellFactory()->createValidCell($cellContext);
			}
		}
	}

	/**
	 * Generates world dimensions.
	 *
	 * @todo Potentially add modified dimension generator to create different structure for world.
	 */
	private function generateWorldDimensions()
	{
		$x = $this->getFieldSize()->getX();
		$y = $this->getFieldSize()->getY();

		$rows        = array_fill(0, $y, null);
		$this->world = array_fill(0, $x, $rows);
	}
}
