<?php

namespace GameOfLife\Component\World;

use IteratorAggregate;
use Traversable;

/**
 * Class WorldIterator
 *
 * Allows simple array style access to whole World matrix.
 */
class WorldIterator implements IteratorAggregate
{
	/**
	 * @var World
	 */
	private $world;

	/**
	 * World constructor.
	 *
	 * @param World $world
	 */
	public function __construct(World $world)
	{
		$this->world = $world;
	}

	/**
	 * Retrieve an external iterator
	 * @link  http://php.net/manual/en/iteratoraggregate.getiterator.php
	 * @return Traversable An instance of an object implementing <b>Iterator</b> or
	 * <b>Traversable</b>
	 * @since 5.0.0
	 */
	public function getIterator()
	{
		$x = $this->world->getFieldSize()->getX();
		$y = $this->world->getFieldSize()->getY();

		for ($i = 0; $i < $x; $i++)
		{
			for ($j = 0; $j < $y; $j++)
			{
				yield $this->world->manipulate($i, $j);
			}
		}
	}
}
