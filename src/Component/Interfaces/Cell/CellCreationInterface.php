<?php

namespace GameOfLife\Component\Interfaces\Cell;

use GameOfLife\Component\Cell\Cell;
use GameOfLife\Component\Cell\CellContext;

interface CellCreationInterface
{
	/**
	 * @param CellContext $cellContext
	 *
	 * @return Cell
	 */
	public function createValidCell(CellContext $cellContext);

	/**
	 * Should create an invalid Cell that cannot be accessed or counted as an object.
	 *
	 * @return Cell
	 */
	public function createInvalidCell();
}
