<?php

namespace GameOfLife\Component\Interfaces\Rule;

interface RuleInterface
{
	/**
	 * Executes the rule checks
	 */
	public function apply();
}
