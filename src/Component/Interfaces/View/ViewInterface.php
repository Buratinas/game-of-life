<?php

namespace GameOfLife\Component\Interfaces\View;

interface ViewInterface
{
	/**
	 * Returns desired results in a specific format.
	 *
	 * @return mixed
	 */
	public function view();
}
