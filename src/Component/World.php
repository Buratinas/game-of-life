<?php

namespace GameOfLife\Component;

class World
{
	const WORLD_CELL_EMPTY = 0;

	private $fieldSize;

	private $world;

	public function __construct(FieldSize $fieldSize)
	{
		$this->fieldSize = $fieldSize;
		$x               = $fieldSize->getX();
		$y               = $fieldSize->getY();

		$rows        = array_fill(0, $x, self::WORLD_CELL_EMPTY);
		$this->world = array_fill(0, $y, $rows);
	}

	/**
	 * Renders the world
	 *
	 * @return array
	 */
	public function show()
	{
		return $this->world;
	}
}
