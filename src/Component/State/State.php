<?php

namespace GameOfLife\Component\State;

abstract class State
{
	/**
	 * Returns instance of the extended class.
	 *
	 * @return static
	 */
	final static public function state()
	{
		return new static();
	}

	/**
	 * @return string
	 */
	public function getState()
	{
		return static::class;
	}
}

