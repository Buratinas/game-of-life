<?php

namespace GameOfLife\Component\Cell;

use Closure;
use GameOfLife\Component\State\State;
use GameOfLife\Component\Traits\InstanceClosureTrait;

class NeighboringCell extends Cell
{
	use InstanceClosureTrait;

	/**
	 * @var Closure
	 */
	private $southNeighbor;

	/**
	 * @var Closure
	 */
	private $westNeighbor;

	/**
	 * @var Closure
	 */
	private $eastNeighbor;

	/**
	 * @var Closure
	 */
	private $northNeighbor;

	/**
	 * @return Closure
	 */
	public function getSouthNeighbor()
	{
		return $this->southNeighbor;
	}

	/**
	 * @return Closure
	 */
	public function getWestNeighbor()
	{
		return $this->westNeighbor;
	}

	/**
	 * @return Closure
	 */
	public function getEastNeighbor()
	{
		return $this->eastNeighbor;
	}

	/**
	 * @return Closure
	 */
	public function getNorthNeighbor()
	{
		return $this->northNeighbor;
	}

	/**
	 * @param Closure $neighborClosure
	 */
	public function addSouthConnection(Closure $neighborClosure)
	{
		$this->southNeighbor = $neighborClosure;
	}

	/**
	 * @param Closure $neighborClosure
	 */
	public function addNorthConnection(Closure $neighborClosure)
	{
		$this->northNeighbor = $neighborClosure;

	}

	/**
	 * @param Closure $neighborClosure
	 */
	public function addEastConnection(Closure $neighborClosure)
	{
		$this->eastNeighbor = $neighborClosure;
	}

	/**
	 * @param Closure $neighborClosure
	 */
	public function addWestConnection(Closure $neighborClosure)
	{
		$this->westNeighbor = $neighborClosure;
	}

	/**
	 * Returns all neighbors with given state
	 *
	 * @param State $state
	 *
	 * @return array
	 */
	public function getNeighborsWithState(State $state)
	{
		$neighbors = [$this->southNeighbor, $this->westNeighbor, $this->eastNeighbor, $this->northNeighbor];

		return array_filter($neighbors, function ($neighbor) use ($state) {
			while ($neighbor instanceof Closure)
			{
				$neighbor = $neighbor();
			}

			return $neighbor instanceof NeighboringCell && $neighbor->getState() == $state;
		});
	}
}
