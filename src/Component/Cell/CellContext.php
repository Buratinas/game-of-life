<?php

namespace GameOfLife\Component\Cell;

use GameOfLife\Component\State\Dead;
use GameOfLife\Component\State\State;
use GameOfLife\Component\World\World;
use GameOfLife\Component\World\WorldCoordinates;

class CellContext
{
	/**
	 * @var State
	 */
	private $state;

	/**
	 * @var World
	 */
	private $world;

	/**
	 * @var WorldCoordinates
	 */
	private $worldCoordinates;

	/**
	 * CellContext constructor.
	 *
	 * @param World            $world
	 * @param WorldCoordinates $worldCoordinates
	 */
	public function __construct(World $world, WorldCoordinates $worldCoordinates, State $state = null)
	{
		if ($state === null)
		{
			$state = Dead::state();
		}
		$this->setState($state);
		$this->setWorld($world);
		$this->setWorldCoordinates(clone $worldCoordinates);
	}

	/**
	 * @return State
	 */
	public function getState()
	{
		return $this->state;
	}

	/**
	 * @param State $state
	 *
	 * @return CellContext
	 */
	public function setState($state)
	{
		$this->state = $state;

		return $this;
	}

	/**
	 * @return World
	 */
	public function getWorld()
	{
		return $this->world;
	}

	/**
	 * @param World $world
	 *
	 * @return CellContext
	 */
	public function setWorld($world)
	{
		$this->world = $world;

		return $this;
	}

	/**
	 * @return WorldCoordinates
	 */
	public function getWorldCoordinates()
	{
		return $this->worldCoordinates;
	}

	/**
	 * @param WorldCoordinates $worldCoordinates
	 *
	 * @return CellContext
	 */
	public function setWorldCoordinates($worldCoordinates)
	{
		$this->worldCoordinates = $worldCoordinates;

		return $this;
	}
}
