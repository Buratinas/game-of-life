<?php

namespace GameOfLife\Component\Cell;

use GameOfLife\Component\State\Alive;
use GameOfLife\Component\State\Dead;
use GameOfLife\Component\State\State;

class Cell
{
	/**
	 * @var State
	 */
	private $state;

	/**
	 * @param State $state
	 */
	public function setState($state)
	{
		$this->state = $state;
	}

	public function __construct(State $state)
	{
		$this->state = $state;
	}

	/**
	 * @return State
	 */
	public function getState()
	{
		return $this->state;
	}

	/**
	 * @return Dead|State
	 */
	public function destroy()
	{
		$this->state = new Dead();

		return $this->state;
	}

	/**
	 * @return Alive|State
	 */
	public function live()
	{
		$this->state = new Alive();

		return $this->state;
	}
}
