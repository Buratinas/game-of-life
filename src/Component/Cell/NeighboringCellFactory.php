<?php

namespace GameOfLife\Component\Cell;

use Closure;
use GameOfLife\Component\Interfaces\Cell\CellCreationInterface;
use GameOfLife\Component\State\Invalid;
use GameOfLife\Component\World\World;

class NeighboringCellFactory implements CellCreationInterface
{
	/**
	 * Creates NeighboringCell with 4 neighbors (north, south, east, west) with Dead::state().
	 *
	 * @param CellContext $cellContext
	 *
	 * @return NeighboringCell
	 */
	public function createValidCell(CellContext $cellContext)
	{
		/**           N
		 * 0,0 -----------------> Y
		 *  |
		 *  |
		 *  |
		 *W |         A             E
		 *  |
		 *  |
		 *  v
		 *  X
		 *            S
		 *  A(5, 5):
		 *  N(4, 5); -1, 0
		 *  S(6, 5); 1, 0
		 *  E(5, 6); 0, 1
		 *  W(5, 4); 0, -1
		 */
		$cell        = new NeighboringCell($cellContext->getState());
		$world       = $cellContext->getWorld();
		$coordinates = $cellContext->getWorldCoordinates();
		$x           = $coordinates->getX();
		$y           = $coordinates->getY();

		$world->manipulate($x, $y)->setState($cellContext->getState());
		$cell->addNorthConnection($this->getCellInstanceClosureAt($world, $x - 1, $y));
		$cell->addSouthConnection($this->getCellInstanceClosureAt($world, $x + 1, $y));
		$cell->addEastConnection($this->getCellInstanceClosureAt($world, $x, $y + 1));
		$cell->addWestConnection($this->getCellInstanceClosureAt($world, $x, $y - 1));

		return $cell;
	}

	/**
	 * Creates invalid cell using anonymous class closure extending NeighboringCell with Invalid::state().
	 *
	 * @return NeighboringCell
	 */
	public function createInvalidCell()
	{
		return new class (Invalid::state()) extends NeighboringCell
		{
		};
	}

	/**
	 * Returns closure for Cell::class instance.
	 * $world at this point might be empty
	 * (i.e. 1st Cell generation lacks accesss to neighbors)
	 *
	 * @param World $world
	 * @param int   $x
	 * @param int   $y
	 *
	 * @return Closure
	 */
	private function getCellInstanceClosureAt(World $world, $x, $y)
	{
		return function () use ($world, $x, $y)
		{
			return $world->manipulate($x, $y)->getInstanceClosure();
		};
	}
}
