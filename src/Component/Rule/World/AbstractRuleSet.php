<?php

namespace GameOfLife\Component\Rule\World;

use Closure;

abstract class AbstractRuleSet
{
	/**
	 * @var Closure[]
	 */
	private $ruleEnforcerClosures;

	/**
	 * @param string  $key
	 * @param Closure $rule
	 */
	public function addRule($key, Closure $rule = null)
	{
		if ($rule === null)
		{
			$rule = function () {};
		}

		$this->ruleEnforcerClosures[$key] = $rule;
	}

	/**
	 * @param $name
	 *
	 * @return Closure
	 */
	public function getRule($name)
	{
		return $this->ruleEnforcerClosures[$name];
	}
}
