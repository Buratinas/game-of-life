<?php

namespace GameOfLife\Component\Rule\World;

use GameOfLife\Component\Cell\NeighboringCell;
use GameOfLife\Component\State\Alive;
use GameOfLife\Component\State\Dead;

class WorldRuleSet extends AbstractRuleSet
{
	/** Any live cell with fewer than two live neighbours dies, as if caused by underpopulation. */
	const RULE_WORLD_DIE_LESS_THEN_TWO_NEIGHBORS = 'less_than_two_neighbors_die';
	/** Any live cell with two or three live neighbours lives on to the next generation */
	const RULE_WORLD_LIVE_IF_TWO_THREE_NEIGHBORS = 'two_three_neighbors_live';
	/** Any live cell with more than three live neighbours dies, as if by overpopulation. */
	const RULE_WORLD_DIE_IF_THREE_PLUS_NEIGHBORS = 'three_plus_neighbors_die';
	/** Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction. */
	const RULE_WORLD_REVIVE_IF_THREE_NEIGHBORS = 'three_exact_neighbors_revive';

	public function __construct()
	{
		/**
		 * RULE: Fewer than live 2 Alive neighbors -> die
		 *
		 * @param NeighboringCell $cell
		 */
		$lessThenTwoNeighborsDie = function (NeighboringCell $cell)
		{
			$alive = Alive::state();

			if ($cell->getState() == $alive && 2 > count($cell->getNeighborsWithState($alive)))
			{
				$cell->destroy();
			}
		};
		$this->addRule(self::RULE_WORLD_DIE_LESS_THEN_TWO_NEIGHBORS, $lessThenTwoNeighborsDie);

		/**
		 * RULE: More than 3 Alive neighbors -> die
		 *
		 * @param NeighboringCell $cell
		 */
		$moreThenThreeNeighborsDie = function (NeighboringCell $cell)
		{
			$alive = Alive::state();

			if ($cell->getState() == $alive && 3 < count($cell->getNeighborsWithState($alive)))
			{
				$cell->destroy();
			}
		};
		$this->addRule(self::RULE_WORLD_DIE_IF_THREE_PLUS_NEIGHBORS, $moreThenThreeNeighborsDie);

		/**
		 * RULE: Dead Cell with 3+ Alive neigehbors -> live
		 *
		 * @param NeighboringCell $cell
		 */
		$threePlusNeighborsRevive = function (NeighboringCell $cell)
		{
			$dead  = Dead::state();
			$alive = Alive::state();

			if ($cell->getState() == $dead && 3 > count($cell->getNeighborsWithState($alive)))
			{
				$cell->live();
			}
		};
		$this->addRule(self::RULE_WORLD_REVIVE_IF_THREE_NEIGHBORS, $threePlusNeighborsRevive);

		/**
		 * RULE: Dead Cell with 3+ Alive neigehbors -> live
		 * Example of combining rules.
		 *
		 * @param NeighboringCell $cell
		 */
		$twoThreeNeighborsSurvive = function (NeighboringCell $cell) use ($lessThenTwoNeighborsDie, $moreThenThreeNeighborsDie)
		{
			$lessThenTwoNeighborsDie->call($this, $cell);
			$moreThenThreeNeighborsDie->call($this, $cell);
		};
		$this->addRule(self::RULE_WORLD_LIVE_IF_TWO_THREE_NEIGHBORS, $twoThreeNeighborsSurvive);
	}
}
