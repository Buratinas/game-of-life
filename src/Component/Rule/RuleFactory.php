<?php

namespace GameOfLife\Component\Rule;

use Closure;
use GameOfLife\Component\Rule\World\AbstractRuleSet;

class RuleFactory
{
	/**
	 * @var AbstractRuleSet
	 */
	private $ruleSetProvider;

	public function __construct(AbstractRuleSet $ruleSet)
	{
		$this->ruleSetProvider = $ruleSet;
	}

	/**
	 * @param $key
	 *
	 * @return Closure
	 */
	public function create($key)
	{
		return $this->ruleSetProvider->getRule($key);
	}
}
