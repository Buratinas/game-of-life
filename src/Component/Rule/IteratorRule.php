<?php

namespace GameOfLife\Component\Rule;

use Closure;
use GameOfLife\Component\Interfaces\Rule\RuleInterface;
use IteratorAggregate;

class IteratorRule implements RuleInterface
{
	/**
	 * @var IteratorAggregate
	 */
	private $iterator;

	/**
	 * @var Closure
	 */
	private $ruleEnforcer;

	/**
	 * IteratorRule constructor.
	 *
	 * @param IteratorAggregate $iteratorAggregate
	 * @param Closure           $ruleEnforcer
	 */
	public function __construct(IteratorAggregate $iteratorAggregate, Closure $ruleEnforcer)
	{
		$this->iterator     = $iteratorAggregate;
		$this->ruleEnforcer = $ruleEnforcer;
	}

	/**
	 * Pass iterated object to rule enforcer for check execution.
	 */
	public function apply()
	{
		foreach ($this->iterator as $item)
		{
			$this->ruleEnforcer->call($this, $item);
		}
	}
}
