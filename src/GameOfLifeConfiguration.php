<?php

namespace GameOfLife;

use GameOfLife\Component\FieldSize;
use GameOfLife\Component\Interfaces\Cell\CellCreationInterface;
use GameOfLife\Component\Rule\World\AbstractRuleSet;

class GameOfLifeConfiguration
{
	/** @var FieldSize */
	private $fieldSize;

	/** @var CellCreationInterface */
	private $cellFactory;

	/** @var AbstractRuleSet */
	private $ruleSet;

	/**
	 * Sets field size for the playing field.
	 *
	 * @param FieldSize $fieldSize
	 */
	public function setFieldSize(FieldSize $fieldSize)
	{
		$this->fieldSize = $fieldSize;
	}

	/**
	 * @return FieldSize
	 */
	public function getFieldSize()
	{
		return $this->fieldSize;
	}

	/**
	 * @param $cellFactory
	 */
	public function setCellFactory($cellFactory)
	{
		$this->cellFactory = $cellFactory;
	}

	/**
	 * @return CellCreationInterface
	 */
	public function getCellFactory()
	{
		return $this->cellFactory;
	}

	/**
	 * @param AbstractRuleSet $ruleSet
	 */
	public function setRuleSet(AbstractRuleSet $ruleSet)
	{
		$this->ruleSet = $ruleSet;
	}

	/**
	 * @return AbstractRuleSet
	 */
	public function getRuleSet()
	{
		return $this->ruleSet;
	}
}
