<?php

namespace GameOfLife\Api;

use GameOfLife\Component\Cell\NeighboringCell;
use GameOfLife\Component\Cell\NeighboringCellFactory;
use GameOfLife\Component\FieldSize;
use GameOfLife\Component\GameOfLifeEngine;
use GameOfLife\Component\Rule\World\WorldInstantiationRuleSet;
use GameOfLife\Component\State\Dead;
use GameOfLife\Component\State\Invalid;
use GameOfLife\Component\World\World;
use GameOfLife\Component\World\WorldView;
use GameOfLife\GameOfLifeConfiguration;

class GameOfLifeApi implements GameOfLifeApiInterface
{
	use HardDependencyTrait;

	/**
	 * @var GameOfLifeConfiguration
	 */
	private $configuration;

	/**
	 * @var World
	 */
	private $world;

	/**
	 * GameOfLifeApi constructor.
	 *
	 * @param GameOfLifeConfiguration|null $configuration
	 */
	public function __construct(GameOfLifeConfiguration $configuration = null)
	{
		$this->configuration = $configuration;
	}

	/**
	 * Returns current world view.
	 *
	 * @return string
	 */
	public function getWorldView(): string
	{
		$worldViewCellClosure = function (&$output) {
			return function (NeighboringCell $cell) use (&$output)
			{
				if ($output === null)
				{
					$output = '';
				}
				$output       .= $cell->getState() == Dead::state() ? 'D' : 'A';
				$eastNeighbor = $cell->getEastNeighbor();
				
				$neighbor     = $eastNeighbor();
				/** @var NeighboringCell $cell */
				$state = ($neighbor instanceof \Closure) ? $neighbor()->getState() : $neighbor->getState();
				if ($state == Invalid::state())
				{
					$output .= PHP_EOL;
				}
			};
		};

		$worldView            = new WorldView($this->world, $worldViewCellClosure);

		return $worldView->view();
	}

	/**
	 * @return array
	 */
	public function getWorldArray()
	{
		$x = 0; $y = 0;
		$worldViewCellClosure = function (&$output) use ($x, $y) {
			return function (NeighboringCell $cell) use (&$output, &$x, &$y)
			{
				if ($output === null)
				{
					$output = [];
				}
				$output[$x][$y++] = $cell->getState() == Dead::state() ? 'D' : 'A';
				$eastNeighbor = $cell->getEastNeighbor();
				$neighbor     = $eastNeighbor();
				/** @var NeighboringCell $cell */
				$cell = $neighbor();
				if ($cell->getState() == Invalid::state())
				{
					$x++;
					$y = 0;
				}
			};
		};

		$worldView            = new WorldView($this->world, $worldViewCellClosure);

		return $worldView->view();
	}

	/**
	 * Creates Game world and returns it's view.
	 *
	 * @param int $width
	 * @param int $height
	 *
	 * @return array
	 */
	public function createWorld(int $width, int $height): array
	{
		$this->world = new World(new FieldSize($width, $height), new NeighboringCellFactory());

		return $this->getWorldArray();
	}

	/**
	 * Updates world with specific view.
	 *
	 * @param string $expectedView
	 *
	 * @return array
	 *
	 * @throws \InvalidArgumentException
	 */
	public function updateWorld(string $expectedView): array
	{
		$this->cantBeNull($this->configuration);
		$fieldSize = $this->extractWorldViewFieldSize($expectedView);

		$ruleSet = new WorldInstantiationRuleSet($this->convertWorldViewToArray($expectedView));
		$this->configuration->setRuleSet($ruleSet);
		$this->configuration->setFieldSize($fieldSize);

		$engine = new GameOfLifeEngine($this->configuration);
		$engine->applyRule(WorldInstantiationRuleSet::RULE_UPDATE_WORLD_FROM_INPUT);
		$this->world = $engine->getWorld();

		return $this->getWorldArray();
	}

	/**
	 * Converts World view string to array (PHP_EOL sanitized).
	 *
	 * @return array
	 */
	private function convertWorldViewToArray(string $worldView)
	{
		$separator = substr($worldView, -1);
		return str_split(str_replace($separator, '', $worldView));
	}

	/**
	 * @param string $worldView
	 *
	 * @return FieldSize
	 */
	private function extractWorldViewFieldSize(string $worldView)
	{
		//@todo inject separator or make it modifyable
		$separator = substr($worldView, -1);
		$worldLines = explode($separator, rtrim($worldView, $separator));
		$height     = count($worldLines);
		$width      = strlen(current($worldLines));

		return new FieldSize($height, $width);
	}
}
