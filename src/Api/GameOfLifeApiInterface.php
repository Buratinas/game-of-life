<?php

namespace GameOfLife\Api;

interface GameOfLifeApiInterface
{
	/**
	 * Creates Game world and returns it's view.
	 *
	 * @param int $width
	 * @param int $height
	 *
	 * @return array|string
	 */
	public function createWorld(int $width, int $height): array;

	/**
	 * Returns current world view.
	 *
	 * @return string
	 */
	public function getWorldView(): string;

	/**
	 * Updates world with specific view.
	 *
	 * @param string $inputView
	 *
	 * @return string
	 */
	public function updateWorld(string $inputView): array;
}
