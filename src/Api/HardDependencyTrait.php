<?php

namespace GameOfLife\Api;

use InvalidArgumentException;

trait HardDependencyTrait
{
	/**
	 * Object can't be null.
	 *
	 * @param mixed $object
	 *
	 * @throws InvalidArgumentException
	 */
	public function cantBeNull($object)
	{
		if ($object === null)
		{
			throw new InvalidArgumentException("Can't be null");
		}
	}

	/**
	 * Array must contain specific key.
	 *
	 * @param $key
	 * @param $array
	 */
	private function mustBeInArray($key, $array)
	{
		if (!isset($array[$key]))
		{
			throw new InvalidArgumentException("Array did not contain required value!");
		}
	}
}
