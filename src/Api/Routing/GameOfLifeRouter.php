<?php

namespace GameOfLife\Api\Routing;

use Closure;
use GameOfLife\Api\GameOfLifeApi;
use GameOfLife\Api\HardDependencyTrait;
use GameOfLife\GameOfLifeConfiguration;

/**
 * Class GameOfLifeRouter
 * Game of Life router containing initial routes.
 *
 * @package GameOfLife\Api\Routing
 */
class GameOfLifeRouter implements ApiCommunicationInterface
{
	use RouteStorageTrait;
	use HardDependencyTrait;

	/**
	 * Create world and return its view.
	 */
	const ROUTE_CREATE_WORLD = '/world/create/:x/:y';

	/**
	 * Update world and return its view.
	 */
	const ROUTE_UPDATE_WORLD = '/world/update/:view';

	public function __construct(GameOfLifeConfiguration $configuration)
	{
		$this->addRoute(
			self::ROUTE_CREATE_WORLD, function (int $x, int $y) use ($configuration)
		{
			$api = new GameOfLifeApi($configuration);

			return $api->createWorld($x, $y);
		}
		);
		$this->addRoute(
			self::ROUTE_UPDATE_WORLD, function (string $expectedView) use ($configuration)
		{
			$api = new GameOfLifeApi($configuration);

			return $api->updateWorld($expectedView);
		}
		);
	}

	/**
	 * Registers a specific route
	 *
	 * @param string  $routeName
	 * @param Closure $executable
	 */
	public function register(string $routeName, Closure $executable)
	{
		$this->addRoute($routeName, $executable);
	}

	/**
	 * Calls designated route. Defined Closure is executed.
	 *
	 * @param string $routeName
	 * @param array  $arguments
	 *
	 * @return mixed
	 */
	public function call(string $routeName, array $arguments = [])
	{
		$this->mustBeInArray($routeName, $this->routes());

		$closure = $this->getRoute($routeName);

		return call_user_func_array($closure, $arguments);
	}
}
