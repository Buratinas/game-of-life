<?php

namespace GameOfLife\Api\Routing;

use Closure;
use GameOfLife\Component\FieldSize;

trait RouteStorageTrait
{
	/**
	 * @var Closure[]
	 */
	private static $routes;

	/**
	 * Allows $routes to be accessible by all used classes (theoretically);
	 *
	 * @return Closure[]
	 */
	private function routes()
	{
		if (self::$routes === null)
		{
			self::$routes = [];
		}

		return self::$routes;
	}

	private function addRoute(string $name, Closure $callable)
	{
		self::$routes[$name] = $callable;
	}

	/**
	 * @param string $routeName
	 *
	 * @return Closure
	 */
	private function getRoute(string $routeName)
	{
		return self::$routes[$routeName];
	}

	/**
	 * @return Closure[]
	 */
	public function getRouteList()
	{
		return $this->routes();
	}
}
