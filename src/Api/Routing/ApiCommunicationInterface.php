<?php

namespace GameOfLife\Api\Routing;

use Closure;

interface ApiCommunicationInterface extends ApiRouteListInterface
{
	/**
	 * Registers a specific route
	 *
	 * @param string  $routeName
	 * @param Closure $executable
	 */
	public function register(string $routeName, Closure $executable);

	/**
	 * Calls designated route. Defined Closure is executed.
	 *
	 * @param string $routeName
	 *
	 * @return mixed
	 */
	public function call(string $routeName);
}
