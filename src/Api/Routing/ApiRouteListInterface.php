<?php

namespace GameOfLife\Api\Routing;

interface ApiRouteListInterface
{
	/**
	 * Returns route list that can be called.
	 *
	 * @return array
	 */
	public function getRouteList();
}
