<?php

namespace GameOfLife\Tests\Component\State;

use GameOfLife\Component\State\Alive;
use GameOfLife\Component\State\Dead;
use GameOfLife\Component\State\State;
use PHPUnit\Framework\TestCase;

class StateTest extends TestCase
{
	public function stateProvider()
	{
		return [
			[Alive::class],
			[Dead::class],
		];
	}

	/**
	 * @dataProvider stateProvider
	 *
	 * @param string $classToTest
	 */
	public function testStateGetStateEqualsItself($classToTest)
	{
		/** @var State $state */
		$state = new $classToTest();

		$stateValue = $state->getState();

		$this->assertTrue($state instanceof $stateValue);
	}
}
