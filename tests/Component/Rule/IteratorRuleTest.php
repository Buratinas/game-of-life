<?php

namespace Tests\GameOfLife\Component\Rule;

use Closure;
use GameOfLife\Component\Cell\NeighboringCell;
use GameOfLife\Component\Rule\IteratorRule;
use GameOfLife\Component\State\Alive;
use GameOfLife\Component\State\Dead;
use GameOfLife\Component\World\WorldIterator;
use GameOfLife\Tests\Helper\Traits\CreateWorldTrait;
use PHPUnit\Framework\TestCase;

class IteratorRuleTest extends TestCase
{
	use CreateWorldTrait;

	/** Randomize every cell state */
	const RULE_WORLD_LIVE_DIE_RANDOMIZE = 'randomize_all';

	/** Any live cell with fewer than two live neighbours dies, as if caused by underpopulation. */
	const RULE_WORLD_DIE_LESS_THEN_TWO_NEIGHBORS = 'less_than_two_neighbors_die';
	/** Any live cell with two or three live neighbours lives on to the next generation */
	const RULE_WORLD_LIVE_IF_TWO_THREE_NEIGHBORS = 'two_three_neighbors_live';
	/** Any live cell with more than three live neighbours dies, as if by overpopulation. */
	const RULE_WORLD_DIE_IF_THREE_PLUS_NEIGHBORS = 'three_plus_neighbors_die';
	/** Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction. */
	const RULE_WORLD_REVIVE_IF_THREE_NEIGHBORS = 'three_exact_neighbors_revive';

	/**
	 * @var Closure[]
	 */
	private $ruleEnforcerClosures = [];

	public function setUp()
	{
		/**
		 * RULE: Fewer than live 2 Alive neighbors -> die
		 *
		 * @param NeighboringCell $cell
		 */
		$this->ruleEnforcerClosures[self::RULE_WORLD_DIE_LESS_THEN_TWO_NEIGHBORS] = function (NeighboringCell $cell)
		{
			$alive = Alive::state();

			if ($cell->getState() == $alive && 2 < count($cell->getNeighborsWithState($alive)))
			{
				$cell->destroy();
			}
		};

		/**
		 * RULE: Randomize whether Cell lives or dies (Hint: start world generation)
		 *
		 * @param NeighboringCell $cell
		 */
		$this->ruleEnforcerClosures[self::RULE_WORLD_LIVE_DIE_RANDOMIZE] = function (NeighboringCell $cell)
		{
			rand(0, 100) > 50 ? $cell->destroy() : $cell->live();
		};

		/**
		 * RULE: More than 3 Alive neighbors -> die
		 *
		 * @param NeighboringCell $cell
		 */
		$this->ruleEnforcerClosures[self::RULE_WORLD_DIE_IF_THREE_PLUS_NEIGHBORS] = function (NeighboringCell $cell)
		{
			$alive = Alive::state();

			if ($cell->getState() == $alive && 3 > count($cell->getNeighborsWithState($alive)))
			{
				$cell->destroy();
			}
		};

		/**
		 * RULE: Dead Cell with 3+ Alive neigehbors -> live
		 *
		 * @param NeighboringCell $cell
		 */
		$this->ruleEnforcerClosures[self::RULE_WORLD_REVIVE_IF_THREE_NEIGHBORS] = function (NeighboringCell $cell)
		{
			$dead  = Dead::state();
			$alive = Alive::state();

			if ($cell->getState() == $dead && 3 > count($cell->getNeighborsWithState($alive)))
			{
				$cell->live();
			}
		};
	}

	public function testRule()
	{
		$world = $this->createWorld();

		$randomizeStart                  = $this->getRuleEnforcerClosure(self::RULE_WORLD_LIVE_DIE_RANDOMIZE);
		$dieWithLessThanTwoLiveNeighbors = $this->getRuleEnforcerClosure(self::RULE_WORLD_DIE_LESS_THEN_TWO_NEIGHBORS);

		$worldIterator = new WorldIterator($world);

		$dieWithLessThanTwoLiveNeighborsRule = new class ($worldIterator, $dieWithLessThanTwoLiveNeighbors) extends IteratorRule
		{
		};

		$randomStartRule = new class ($worldIterator, $randomizeStart) extends IteratorRule
		{
		};

		$randomStartRule->apply();

		$firstView = $world->view();

		$dieWithLessThanTwoLiveNeighborsRule->apply();

		$secondView = $world->view();

		$this->assertNotEquals($firstView, $secondView);
	}

	public function testCombinedRuleUsage()
	{
		$world         = $this->createWorld();
		$worldIterator = new WorldIterator($world);

		$randomizeStart          = $this->getRuleEnforcerClosure(self::RULE_WORLD_LIVE_DIE_RANDOMIZE);
		$reviveThreePlusNeihbors = $this->getRuleEnforcerClosure(self::RULE_WORLD_REVIVE_IF_THREE_NEIGHBORS);
		$dieThreePlusNeighbors   = $this->getRuleEnforcerClosure(self::RULE_WORLD_DIE_IF_THREE_PLUS_NEIGHBORS);
		$dieLessThanTwoNeighbors = $this->getRuleEnforcerClosure(self::RULE_WORLD_DIE_LESS_THEN_TWO_NEIGHBORS);

		/**
		 * Game of Life rule list chek
		 *
		 * @param NeighboringCell $cell
		 */
		$combinedExistanceRuleEnforcer = function (NeighboringCell $cell) use (
			$reviveThreePlusNeihbors,
			$dieLessThanTwoNeighbors,
			$dieThreePlusNeighbors
		)
		{
			$dieLessThanTwoNeighbors($cell);
			$dieThreePlusNeighbors($cell);
			$reviveThreePlusNeihbors($cell);
		};

		$randomStartRule = new class ($worldIterator, $randomizeStart) extends IteratorRule
		{
		};

		$combinedExistanceRule = new class ($worldIterator, $combinedExistanceRuleEnforcer) extends IteratorRule
		{
		};

		$randomStartRule->apply();

		$firstView = $world->view();

		$combinedExistanceRule->apply();

		$secondView = $world->view();

		$this->assertNotEquals($firstView, $secondView);
	}

	/**
	 * Shows how to manipulate world with an array with a simple rule
	 */
	public function testWorldFromArrayCreation()
	{
		$inputWorldView     = $this->inputDeadFaceWorldView;
		$inputWorldArray    = $this->convertWorldViewToArray($inputWorldView);
		$worldFromInputView = $this->createWorld();

		$this->assertNotEquals($inputWorldView, $worldFromInputView->view());

		$counter            = 0;
		$worldFromInputRule = function (NeighboringCell $cell) use ($inputWorldArray, &$counter)
		{
			if ($inputWorldArray[$counter++] === 'A')
			{
				$cell->live();
			}
			else
			{
				$cell->destroy();
			}
		};
		$worldIterator      = new WorldIterator($worldFromInputView);
		$ruleIterator       = new IteratorRule($worldIterator, $worldFromInputRule);
		$ruleIterator->apply();

		$this->assertEquals($inputWorldView, $worldFromInputView->view());
	}

	/**
	 * @param string $id
	 *
	 * @return Closure
	 */
	private function getRuleEnforcerClosure($id)
	{
		return $this->ruleEnforcerClosures[$id];
	}
}

