<?php

namespace Tests\GameOfLife\Component\Rule;

use GameOfLife\Component\Cell\NeighboringCell;
use GameOfLife\Component\Rule\RuleFactory;
use GameOfLife\Component\Rule\World\WorldRuleSet;
use GameOfLife\Component\State\Alive;
use GameOfLife\Component\State\Dead;
use PHPUnit\Framework\TestCase;

class RuleFactoryTest extends TestCase
{
	public function testFactoryRuleExecutesFromSingleRuleSet()
	{
		$cell        = new NeighboringCell(Alive::state());
		$ruleFactory = new RuleFactory(new WorldRuleSet());

		$this->assertEquals(Alive::state(), $cell->getState());
		$rule = $ruleFactory->create(WorldRuleSet::RULE_WORLD_DIE_LESS_THEN_TWO_NEIGHBORS);

		$rule($cell);

		$this->assertEquals(Dead::state(), $cell->getState());
	}
}

