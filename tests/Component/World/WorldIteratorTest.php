<?php

namespace GameOfLife\Tests\Component\State;

use GameOfLife\Component\Cell\Cell;
use GameOfLife\Component\World\WorldIterator;
use GameOfLife\Tests\Helper\Traits\CreateWorldTrait;
use PHPUnit\Framework\TestCase;

class WorldIteratorTest extends TestCase
{
	use CreateWorldTrait;

	public function testWorldIteratorContainsAllCells()
	{
		$world = $this->createWorld();

		$worldIterator = new WorldIterator($world);

		$cells = [];
		foreach ($worldIterator as $cell)
		{
			$cells[] = $cell;
			$this->assertInstanceOf(Cell::class, $cell);
		}

		$fieldSize = $world->getFieldSize();
		$this->assertEquals($fieldSize->getX() * $fieldSize->getY(), count($cells));
	}
}
