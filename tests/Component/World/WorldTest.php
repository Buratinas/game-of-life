<?php

namespace GameOfLife\Tests\Component\State;

use GameOfLife\Component\Cell\Cell;
use GameOfLife\Component\Interfaces\View\ViewInterface;
use GameOfLife\Tests\Helper\Traits\CreateWorldTrait;
use PHPUnit\Framework\TestCase;

class WorldTest extends TestCase
{
	use CreateWorldTrait;

	public function testWorld()
	{
		$world = $this->createWorld();

		$this->assertInstanceOf(ViewInterface::class, $world);

		$this->assertEquals($this->getWorldFilledWith('D'), $world->view());
	}

	public function testWorldManipulateAtCoordinates()
	{
		$world = $this->createWorld();

		$this->assertInstanceOf(Cell::class, $world->manipulate(4, 5));

		$world->manipulate(4, 0)->live();
		$deadWorld = $this->getWorldFilledWith('D');
		$deadWorld[44] = 'A';

		$this->assertEquals($deadWorld, $world->view());
	}

	/**
	 * @param $symbol
	 *
	 * @return string
	 */
	private function getWorldFilledWith($symbol)
	{
		$lineView = join('', array_fill(0, $this->worldWidth, $symbol));

		return join(PHP_EOL, array_fill(0, $this->worldWidth, $lineView)) . PHP_EOL;
	}
}
