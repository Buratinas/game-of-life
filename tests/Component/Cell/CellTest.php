<?php

namespace GameOfLife\Tests\Component\Cell;

use GameOfLife\Component\Cell\Cell;
use GameOfLife\Component\State\Alive;
use GameOfLife\Component\State\Dead;
use PHPUnit\Framework\TestCase;

class CellTest extends TestCase
{
	public function testCell()
	{
		$cell = new Cell(new Alive());

		$this->assertInstanceOf(Alive::class, $cell->getState());
		$this->assertEquals(new Alive(), $cell->getState());
	}

	public function testCellDestroy()
	{
		$cell = new Cell(Alive::state());

		$this->assertEquals(Dead::state(), $cell->destroy());
	}

	public function testCellLive()
	{
		$cell = new Cell(Dead::state());

		$this->assertEquals(Alive::state(), $cell->live());
	}
}
