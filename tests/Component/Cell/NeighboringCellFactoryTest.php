<?php

namespace GameOfLife\Tests\Component\Cell;

use Closure;
use GameOfLife\Component\Cell\CellContext;
use GameOfLife\Component\Cell\NeighboringCellFactory;
use GameOfLife\Component\State\Alive;
use GameOfLife\Component\State\Dead;
use GameOfLife\Component\State\Invalid;
use GameOfLife\Component\World\World;
use GameOfLife\Component\World\WorldCoordinates;
use GameOfLife\Tests\Helper\Traits\CreateWorldTrait;
use PHPUnit\Framework\TestCase;

class NeighboringCellFactoryTest extends TestCase
{
	use CreateWorldTrait;

	public function testNeighboringCellFactoryCreatesInvalidCells()
	{
		$cellFactory = new NeighboringCellFactory();

		$cell = $cellFactory->createInvalidCell();

		$this->assertInstanceOf(Invalid::class, $cell->getState());
	}

	public function testNeighboringCellFactoryCreatesValidCells()
	{
		$cellFactory = new NeighboringCellFactory();
		$cellContext = new CellContext($this->createWorld(), new WorldCoordinates(1, 1));

		$cell = $cellFactory->createValidCell($cellContext);

		$this->assertInstanceOf(Dead::class, $cell->getState());
	}

	/**
	 * @return array
	 */
	public function cellExecutionProvider()
	{

		$testCaseGenerator = function (array $coordinatesToPopulate)
		{
			/**
			 * @param CellContext            $cellContext
			 */
			$createCellClosure = function (CellContext $cellContext)
			{
				$cellContext->getWorld()->getCellFactory()->createValidCell($cellContext);
			};

			/**
			 * @param World            $world
			 * @param WorldCoordinates $worldCoordinates
			 *
			 * @return CellContext
			 */
			$cellContextClosure = function (World $world, WorldCoordinates $worldCoordinates)
			{
				return new CellContext($world, $worldCoordinates, Alive::state());
			};

			/**
			 * @param NeighboringCellFactory $cellFactory
			 * @param World                  $world
			 * @param WorldCoordinates       $worldCoordinates
			 */
			$populateWorldClosure = function (NeighboringCellFactory $cellFactory, World $world, WorldCoordinates $worldCoordinates) use ($createCellClosure, $cellContextClosure)
			{
				$cellContext = $cellContextClosure($world, $worldCoordinates);
				$createCellClosure($cellContext);
			};

			return function (World $world) use ($coordinatesToPopulate, $populateWorldClosure)
			{
				foreach ($coordinatesToPopulate as $worldCoordinates)
				{
					$populateWorldClosure($world->getCellFactory(), $world, $worldCoordinates);
				}
			};
		};

		$coordinates = new WorldCoordinates(0, 0);

		$cloneCoords = function () use ($coordinates)
		{
			return clone $coordinates;
		};

		$twoCellWorld = [$cloneCoords()->setX(1), $cloneCoords()->setX(2)];
		$oneCompleteNeighborCellWorld = [
			$cloneCoords()->setX(1)->setY(1),
			$cloneCoords()->setX(2)->setY(1),
			$cloneCoords()->setX(0)->setY(1),
			$cloneCoords()->setX(1)->setY(2),
			$cloneCoords()->setX(1)->setY(0),
		];

		$oneCompleteNeighborCell = $testCaseGenerator($oneCompleteNeighborCellWorld);

		return [
			[$testCaseGenerator($twoCellWorld), 1, 0, 2, 1],
			[$oneCompleteNeighborCell, 1, 1, 0, 4],
			[$oneCompleteNeighborCell, 2, 1, 3, 1]
		];
	}

	/**
	 * @dataProvider cellExecutionProvider
	 *
	 * @param Closure $cellModificationClosure
	 * @param int     $x
	 * @param int     $y
	 * @param int     $expectedDeadNeighbors
	 * @param int     $expectedAliveNeighbors
	 */
	public function testNeighboringCellFactoryCreatesValidCellsWithNeighbors(
		Closure $cellModificationClosure,
		$x,
		$y,
		$expectedDeadNeighbors,
		$expectedAliveNeighbors
	) {
		$world       = $this->createWorld();

		$cellModificationClosure($world);

		$neighboringCell = $world->manipulate($x, $y);
		$this->assertEquals($expectedDeadNeighbors, count($neighboringCell->getNeighborsWithState(Dead::state())));
		$this->assertEquals($expectedAliveNeighbors, count($neighboringCell->getNeighborsWithState(Alive::state())));
	}
}
