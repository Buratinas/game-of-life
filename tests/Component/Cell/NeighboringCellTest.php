<?php

namespace GameOfLife\Tests\Component\Cell;

use Closure;
use GameOfLife\Component\Cell\NeighboringCell;
use GameOfLife\Component\State\Alive;
use GameOfLife\Component\State\Dead;
use PHPUnit\Framework\TestCase;

class NeighboringCellTest extends TestCase
{
	/**
	 * @return array
	 */
	public function cellConnectionProvider()
	{

		$aliveNeighbor = new NeighboringCell(Alive::state());
		$deadNeighbor  = new NeighboringCell(Dead::state());

		$aliveSouth = clone $aliveNeighbor;
		$aliveEast  = clone $aliveNeighbor;
		$aliveWest  = clone $aliveNeighbor;
		$aliveNorth = clone $aliveNeighbor;

		$deadSouth = clone $deadNeighbor;
		$deadEast  = clone $deadNeighbor;
		$deadWest  = clone $deadNeighbor;
		$deadNorth = clone $deadNeighbor;

		$destroySouthAndEastModification = function () use ($aliveSouth, $aliveEast)
		{
			$aliveSouth->destroy();
			$aliveEast->destroy();
		};

		$liveWestNorthAndEastModification = function () use ($deadWest, $deadNorth, $deadEast)
		{
			$deadWest->live();
			$deadNorth->live();
			$deadEast->live();
		};

		return [
			[$aliveNorth, $aliveSouth, $aliveEast, $aliveWest, $destroySouthAndEastModification, 4, 2],
			[$deadNorth, $deadSouth, $deadEast, $deadWest, $liveWestNorthAndEastModification, 0, 1]
		];
	}

	/**
	 * @dataProvider cellConnectionProvider
	 *
	 * @param NeighboringCell $north
	 * @param NeighboringCell $south
	 * @param NeighboringCell $east
	 * @param NeighboringCell $west
	 * @param Closure         $modificationClosure
	 * @param int             $aliveCount
	 * @param int             $expectedDeadAfterModification
	 */
	public function testAddNeighborConnection(
		NeighboringCell $north,
		NeighboringCell $south,
		NeighboringCell $east,
		NeighboringCell $west,
		Closure $modificationClosure,
		$aliveCount,
		$expectedDeadAfterModification
	) {
		$cell = new NeighboringCell(Dead::state());

		$cell->addSouthConnection($south->getInstanceClosure());
		$cell->addEastConnection($east->getInstanceClosure());
		$cell->addWestConnection($west->getInstanceClosure());
		$cell->addNorthConnection($north->getInstanceClosure());

		$this->assertEquals($aliveCount, count($cell->getNeighborsWithState(Alive::state())));

		$modificationClosure();
		$this->assertEquals($expectedDeadAfterModification, count($cell->getNeighborsWithState(Dead::state())));
	}

	public function testConnectionWithNoNeighborsReturnsZeroNeighbors()
	{
		$cell = new NeighboringCell(Dead::state());

		$this->assertEquals(0, count($cell->getNeighborsWithState(Dead::state())));
		$this->assertEquals(0, count($cell->getNeighborsWithState(Alive::state())));
	}
}
