<?php

namespace GameOfLife\Tests;

use GameOfLife\Component\Cell\CellContext;
use GameOfLife\Component\Cell\NeighboringCell;
use GameOfLife\Component\FieldSize;
use GameOfLife\Component\GameOfLifeEngine;
use GameOfLife\Component\Rule\IteratorRule;
use GameOfLife\Component\Rule\RuleFactory;
use GameOfLife\Component\Rule\World\AbstractRuleSet;
use GameOfLife\Component\Rule\World\WorldRuleSet;
use GameOfLife\Component\State\Alive;
use GameOfLife\Component\World\World;
use GameOfLife\Component\World\WorldCoordinates;
use GameOfLife\Component\World\WorldIterator;
use GameOfLife\GameOfLifeConfiguration;
use GameOfLife\Tests\Helper\Traits\CreateEngineTrait;
use GameOfLife\Tests\Helper\Traits\CreateWorldTrait;
use PHPUnit\Framework\TestCase;

class GameOfLifeEngineTest extends TestCase
{
	use CreateEngineTrait;
	use CreateWorldTrait;

	/**
	 * Shows how to configure engine with configuration.
	 */
	public function testWorldFromConfiguration()
	{
		$ruleSet = new WorldRuleSet();

		$fieldSize   = new FieldSize(10, 10);
		$cellFactory = $this->createTestCellFactory();

		$configuration = new GameOfLifeConfiguration();
		$configuration->setFieldSize($fieldSize);
		$configuration->setCellFactory($cellFactory);

		$engine = $this->createEngine($fieldSize, $cellFactory, $ruleSet);
		$world  = $engine->createWorld();

		$this->assertEquals($fieldSize, $world->getFieldSize());
		$this->assertEquals($cellFactory, $world->getCellFactory());
	}

	/**
	 * Mirror Factory can be used for specific object creation
	 */
	public function testMirrorFactory()
	{
		$fieldSize         = new FieldSize(10, 10);
		$mirrorWorld       = $this->createWorld();
		$mirrorCellFactory = $this->createStaticMirrorCellFactory($mirrorWorld);

		$configuration = new GameOfLifeConfiguration();
		$configuration->setFieldSize($fieldSize);
		$configuration->setCellFactory($mirrorCellFactory);

		$engine = new GameOfLifeEngine($configuration);

		/** @var World $world */
		$world          = $engine->createWorld();
		$worldFieldSize = $world->getFieldSize();

		$this->assertEquals($fieldSize, $worldFieldSize);
		$this->assertEquals($mirrorCellFactory, $world->getCellFactory());

		// clean upwards -> to trait?

		$context = new CellContext($world, new WorldCoordinates(5, 5));
		$cell    = $mirrorCellFactory->createValidCell($context);

		$clonedCell = $mirrorWorld->manipulate(5, 5);

		$this->assertEquals($cell->getState(), $engine->createWorld()->manipulate(5, 5)->getState());

		// can manipulate with both $cell and $clonedCell - TIP. allows different manipulators to exist on a test level.
		$cell->destroy();
		$clonedCell->live();

		$this->assertEquals($cell->getState(), $engine->createWorld()->manipulate(5, 5)->getState());

		$this->assertEquals($clonedCell->getState(), $engine->createWorld()->manipulate(5, 5)->getState());
		$this->assertEquals(Alive::state(), $engine->createWorld()->manipulate(5, 5)->getState());
	}

	public function testObjectCreation()
	{
		$engine = $this->createDefaultEngine();

		$worldIterator = $engine->createWorldIterator();
		$ruleFactory   = $engine->createRuleFactory();
		$world         = $engine->createWorld();
		$ruleSet       = $engine->getRuleSet();

		self::assertInstanceOf(WorldIterator::class, $worldIterator);
		self::assertInstanceOf(RuleFactory::class, $ruleFactory);
		self::assertInstanceOf(World::class, $world);
		self::assertInstanceOf(AbstractRuleSet::class, $ruleSet);
	}

	/**
	 * An example how Game of Life single move can be implemented.
	 * Random start is used to compare.
	 */
	public function testGameOfLifeRuleIterationChangesWorld()
	{
		$engine = $this->createDefaultEngine();

		$worldIterator = $engine->createWorldIterator();
		$ruleFactory   = $engine->createRuleFactory();
		$world         = $engine->getWorld();

		/**
		 * Game of Life rules
		 */
		$gameRules = [
			WorldRuleSet::RULE_WORLD_DIE_LESS_THEN_TWO_NEIGHBORS,
			WorldRuleSet::RULE_WORLD_DIE_IF_THREE_PLUS_NEIGHBORS,
			WorldRuleSet::RULE_WORLD_REVIVE_IF_THREE_NEIGHBORS
		];

		/**
		 * RULE: Randomize whether Cell lives or dies (Hint: start world generation)
		 *
		 * @param NeighboringCell $cell
		 */
		$start = function (NeighboringCell $cell)
		{
			rand(0, 100) > 50 ? $cell->destroy() : $cell->live();
		};

		$ruleIterator = new IteratorRule($worldIterator, $start);
		$ruleIterator->apply();

		$worldStartView = $world->view();

		foreach ($gameRules as $gameRule)
		{
			$rule         = $ruleFactory->create($gameRule);
			$ruleIterator = new IteratorRule($worldIterator, $rule);
			$ruleIterator->apply();
		}

		$worldAfterStartView = $world->view();

		$this->assertNotEquals($worldAfterStartView, $worldStartView);
	}
}
