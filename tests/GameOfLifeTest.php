<?php

namespace GameOfLife\Tests;

use PHPUnit\Framework\TestCase;
use GameOfLife\Component\World;
use GameOfLife\GameOfLife;
use GameOfLife\GameOfLifeConfiguration;

class GameOfLifeTest extends TestCase
{
	public function testGameIteration()
	{
		$life = $this->createGameOfLife();
		$this->assertEquals(1, $life->iterate());
		$this->assertEquals(2, $life->iterate());
	}

	public function testGamePlay()
	{
		$life = $this->createGameOfLife();
		/** @var World $world */
		$world = $life->play();

		$this->assertEquals([], $world->show());

	}

	public function testGameShowWithoutConfiguredFieldRendersEmptyArray()
	{
		$life = $this->createGameOfLife();
		$this->assertEquals([], $life->display()->show());
	}

	public function testGameShowRendersConfiguredFieldRendersMatrix()
	{
		$x = 2;
		$y = 2;
		$configuration = new GameOfLifeConfiguration();
		$configuration->setFieldSize($x, $y);

		$life = $this->createGameOfLife($configuration);

		$this->assertEquals([[0, 0], [0, 0]], $life->display()->show());
	}

	public function testGameShowReturnsWorldObject()
	{
		$x = 1;
		$y = 1;
		$configuration = new GameOfLifeConfiguration();
		$configuration->setFieldSize($x, $y);

		$life = $this->createGameOfLife($configuration);
		$world = $life->display();

		$this->assertInstanceOf(World::class, $world);
	}

// =========================== Private test methods/helpers ======================

	/**
	 * @param GameOfLifeConfiguration $configuration
	 *
	 * @return GameOfLife
	 */
	private function createGameOfLife(GameOfLifeConfiguration $configuration = null)
	{
		if (!$configuration)
		{
			$configuration = new GameOfLifeConfiguration();
		}

		return new GameOfLife($configuration);
	}
}
