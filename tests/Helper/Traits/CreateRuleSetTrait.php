<?php

namespace GameOfLife\Tests\Helper\Traits;

use GameOfLife\Component\Cell\NeighboringCellFactory;
use GameOfLife\Component\FieldSize;
use GameOfLife\Component\Interfaces\Cell\CellCreationInterface;
use GameOfLife\Component\Rule\World\WorldRuleSet;
use GameOfLife\Component\World\World;

trait CreateRuleSetTrait
{
	/**
	 * @param FieldSize|null             $fieldSize
	 * @param CellCreationInterface|null $cellFactory
	 *
	 * @return WorldRuleSet
	 */
	private function createDefaultWorldRuleSet()
	{
		return new WorldRuleSet();
	}
}
