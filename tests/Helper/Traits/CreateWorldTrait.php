<?php

namespace GameOfLife\Tests\Helper\Traits;

use GameOfLife\Component\Cell\NeighboringCellFactory;
use GameOfLife\Component\FieldSize;
use GameOfLife\Component\Interfaces\Cell\CellCreationInterface;
use GameOfLife\Component\World\World;

trait CreateWorldTrait
{
	private $worldWidth = 10;

	private $worldHeight = 10;

	private $inputWorldView =
		"DDDDDDDDDD" . PHP_EOL .
		"DDDDDDDDDD" . PHP_EOL .
		"DDDDDDDDDD" . PHP_EOL .
		"DDDDDDDDDD" . PHP_EOL .
		"DDDDDDDDDD" . PHP_EOL .
		"DDDDDDDDDD" . PHP_EOL .
		"DDDDDDDDDD" . PHP_EOL .
		"DDDDDDDDDD" . PHP_EOL .
		"DDDDDDDDDD" . PHP_EOL .
		"DDDDDDDDDD" . PHP_EOL ;

	/**
	 * Looks like a face from dead cells :D
	 *
	 * @var string
	 */
	private $inputDeadFaceWorldView =
			"AAAAAAAAAA" . PHP_EOL .
			"AADDAAADDA" . PHP_EOL .
			"ADDDAAADDA" . PHP_EOL .
			"AAAADDAAAA" . PHP_EOL .
			"AAAADDAAAA" . PHP_EOL .
			"AAAAAAAAAA" . PHP_EOL .
			"AADDADDDAA" . PHP_EOL .
			"AAADDDDAAA" . PHP_EOL .
			"AAAADDAAAA" . PHP_EOL .
			"AAAAAAAAAA" . PHP_EOL ;

	/**
	 * @param FieldSize|null             $fieldSize
	 * @param CellCreationInterface|null $cellFactory
	 *
	 * @return World
	 */
	private function createWorld(FieldSize $fieldSize = null, CellCreationInterface $cellFactory = null)
	{
		if ($fieldSize === null)
		{
			$fieldSize = new FieldSize($this->worldWidth, $this->worldHeight);
		}

		if ($cellFactory === null)
		{
			$cellFactory = new NeighboringCellFactory();
		}

		return new World($fieldSize, $cellFactory);
	}

	/**
	 * @return string
	 */
	private function getInputWorldView()
	{
		return $this->inputWorldView;
	}

	/**
	 * @return array
	 */
	private function getInputWorldArray()
	{
		return str_split(str_replace(PHP_EOL, '', $this->inputWorldView));
	}

	/**
	 * Converts World view string to array (PHP_EOL sanitized).
	 *
	 * @param string $worldView
	 *
	 * @return array
	 */
	private function convertWorldViewToArray(string $worldView)
	{
		return str_split(str_replace(PHP_EOL, '', $worldView));
	}
}
