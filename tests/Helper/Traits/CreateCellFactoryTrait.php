<?php

namespace GameOfLife\Tests\Helper\Traits;

use GameOfLife\Component\Cell\Cell;
use GameOfLife\Component\Cell\CellContext;
use GameOfLife\Component\Cell\NeighboringCellFactory;
use GameOfLife\Component\Interfaces\Cell\CellCreationInterface;
use GameOfLife\Component\State\Alive;
use GameOfLife\Component\State\Invalid;
use GameOfLife\Component\World\World;

trait CreateCellFactoryTrait
{
	private static $world;

	/**
	 * Creates Cell Factory which builds cells based on mirrored world.
	 * This enables building specific world builders.
	 *
	 * @param World $world
	 *
	 * @return CellCreationInterface
	 */
	private function createStaticMirrorCellFactory(World $world)
	{
		self::$world = $world;

		return new class ($world) implements CellCreationInterface
		{
			private $mirrorWorld;

			public function __construct(World $mirrorWorld)
			{
				$this->mirrorWorld = $mirrorWorld;
			}

			/**
			 * Returning mirrored cell from the world which can be statically accessed and modified.
			 *
			 * @param CellContext $cellContext
			 *
			 * @return \GameOfLife\Component\Cell\NeighboringCell
			 */
			public function createValidCell(CellContext $cellContext)
			{
				$coordinates = $cellContext->getWorldCoordinates();
				return $this->mirrorWorld->manipulate($coordinates->getX(), $coordinates->getY());
			}

			/**
			 * Bounce back to mirrored world factory implementation.
			 *
			 * @return \GameOfLife\Component\Cell\NeighboringCell
			 */
			public function createInvalidCell()
			{
				return $this->mirrorWorld->getCellFactory()->createInvalidCell();
			}
		};
	}

	/**
	 * @return CellCreationInterface
	 */
	private function createTestCellFactory()
	{
		return new class implements CellCreationInterface
		{
			/**
			 * @param CellContext $cellContext
			 *
			 * @return Cell
			 */
			public function createValidCell(CellContext $cellContext)
			{
				return new Cell(Alive::state());
			}

			/**
			 * @return Cell
			 */
			public function createInvalidCell()
			{
				return new Cell(Invalid::state());
			}
		};
	}

	/**
	 * @return CellCreationInterface
	 */
	private function createTestNeighboringCellFactory()
	{
		return new NeighboringCellFactory();
	}
}
