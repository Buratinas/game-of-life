<?php

namespace GameOfLife\Tests\Helper\Traits;

use GameOfLife\Component\FieldSize;
use GameOfLife\Component\GameOfLifeEngine;
use GameOfLife\Component\Interfaces\Cell\CellCreationInterface;
use GameOfLife\Component\Rule\World\AbstractRuleSet;
use GameOfLife\GameOfLifeConfiguration;

trait CreateEngineTrait
{
	use CreateFieldSizeTrait;
	use CreateCellFactoryTrait;
	use CreateRuleSetTrait;

	/**
	 * Creates Engine for testing with given world FieldSize parameter.
	 *
	 * @param FieldSize             $fieldSize
	 * @param CellCreationInterface $cellFactory
	 * @param AbstractRuleSet       $ruleSet
	 *
	 * @return GameOfLifeEngine
	 */
	private function createEngine(FieldSize $fieldSize, CellCreationInterface $cellFactory, AbstractRuleSet $ruleSet)
	{
		$configuration = new GameOfLifeConfiguration();
		$configuration->setFieldSize($fieldSize);
		$configuration->setCellFactory($cellFactory);
		$configuration->setRuleSet($ruleSet);

		return new GameOfLifeEngine($configuration);
	}

	/**
	 * @return GameOfLifeEngine
	 */
	private function createDefaultEngine()
	{
		$fieldSize   = $this->createDefaultFieldSize();
		$cellFactory = $this->createTestNeighboringCellFactory();
		$ruleSet     = $this->createDefaultWorldRuleSet();

		return $this->createEngine($fieldSize, $cellFactory, $ruleSet);
	}

}
