<?php

namespace GameOfLife\Tests\Helper\Traits;

use GameOfLife\Component\FieldSize;

trait CreateFieldSizeTrait
{
	/**
	 * @var int
	 */
	private $defaultWorldWidth = 10;

	/**
	 * @var int
	 */
	private $defaultWorldHeight = 10;

	/**
	 * Creates Engine for testing with given world FieldSize parameter.
	 *
	 * @param int $height
	 * @param int $width
	 *
	 * @return FieldSize
	 */
	private function createFieldSize(int $height, int $width)
	{
		return new FieldSize($height, $width);
	}

	/**
	 * @return FieldSize
	 */
	private function createDefaultFieldSize()
	{
		return new FIeldSize($this->defaultWorldHeight, $this->defaultWorldWidth);
	}
}
