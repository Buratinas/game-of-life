<?php

namespace GameOfLife\Tests\Component\Cell;

use GameOfLife\Api\GameOfLifeApi;
use GameOfLife\Component\Cell\NeighboringCellFactory;
use GameOfLife\GameOfLifeConfiguration;
use GameOfLife\Tests\Helper\Traits\CreateFieldSizeTrait;
use GameOfLife\Tests\Helper\Traits\CreateWorldTrait;
use PHPUnit\Framework\TestCase;
use Psr\Log\InvalidArgumentException;

class GameOfLifeApiTest extends TestCase
{
	use CreateWorldTrait;
	use CreateFieldSizeTrait;

	/**
	 * Api example.
	 * We don't know what it will be used by, so:
	 * - we expect only certain inputs and outputs.
	 */
	public function testCreateWorld()
	{
		$gameOfLifeApi = new GameOfLifeApi();
		$expectedView  = $this->getInputWorldView();

		$view = $gameOfLifeApi->createWorld($this->worldWidth, $this->worldHeight);

		$expectedArray = $this->convertViewStringToArray($expectedView);

		$this->assertEquals($expectedArray, $view);
	}

	/**
	 * Api example.
	 * How to update world.
	 */
	public function testCreateWorldFromInput()
	{
		$configuration = new GameOfLifeConfiguration();
		$configuration->setCellFactory(new NeighboringCellFactory());
		$gameOfLifeApi = new GameOfLifeApi($configuration);
		$expectedView  = $this->inputDeadFaceWorldView;

		$gameOfLifeApi->createWorld($this->worldWidth, $this->worldHeight);
		$view = $gameOfLifeApi->updateWorld($expectedView);

		$expectedArray = $this->convertViewStringToArray($expectedView);

		$this->assertEquals($expectedArray, $view);
	}

	/**
	 * @expectedException InvalidArgumentException
	 */
	public function testConfigurationHardDependencyMissing()
	{
		$gameOfLifeApi = new GameOfLifeApi();
		$gameOfLifeApi->updateWorld("");
	}

	/**
	 * @param string $expectedView
	 *
	 * @return array
	 */
	private function convertViewStringToArray($expectedView)
	{
		$worldLines = explode(PHP_EOL, rtrim($expectedView, "|" . PHP_EOL));
		foreach ($worldLines as &$cellList)
		{
			$cellList = str_split($cellList);
		}

		return $worldLines;
	}
}
